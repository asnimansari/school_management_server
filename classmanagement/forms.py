from django import forms
from .models import SchoolClass
from django.contrib.auth.models import User
from usermanagement.models import Profile

from .models import *
from general.constants import *


BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
FAVORITE_COLORS_CHOICES = (
    ('blue', 'Blue'),
    ('green', 'Green'),
    ('black', 'Black'),
)

class_name_choice = [('LKG','LKG'),('UKG','UKG'),('I','I'),('II','II'),('III','III'),('IV','IV'),('V','V'),('VI','VI'),('VII','VII'),('VIII','VIII'),('IX','IX'),('X','X'),('XI','XI'),('XII','XII')]#,'I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII')



class SchoolClassForm(forms.ModelForm):
	class_teacher = forms.ModelChoiceField(queryset=Profile.objects.filter(who_is_this = TEACHER), empty_label="(Nothing)")
	class_name = forms.ChoiceField(choices = class_name_choice);
	class Meta:
		model = SchoolClass
		fields = ('class_teacher','starting_year','ending_year','class_name','division_name')


class StudentClassAllocationForm(forms.Form):
	def __init__(self,i):

		class_number = forms.ModelChoiceField(queryset = SchoolClass.objects.filter(id = i))