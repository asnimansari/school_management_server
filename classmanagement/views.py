from django.shortcuts import render

# from django.http import HttpResponse
from django.http import HttpResponse,HttpResponseRedirect


from .forms import *
from .models import *
import datetime as d

# Create your views here.
def addnewclass(request):
	if request.method == "GET":
		val_dic = {}
		# schoolClassForm = SchoolClassForm()
		schoolClassForm = SchoolClassForm(initial = {'starting_year':d.datetime.now().year,'ending_year':d.datetime.now().year + 1})

		val_dic['schoolClassForm'] = schoolClassForm
		return render(request,'cm/newclass.html',val_dic)


		
	if request.method == "POST":
		val_dic = {}
		val_dic['errors'] = []
		schoolClassForm = SchoolClassForm(request.POST)
		if schoolClassForm.is_valid():
			schoolClass = schoolClassForm.save(commit = False)
			schoolClass.save()

		val_dic['schoolClassForm'] = schoolClassForm
		return HttpResponseRedirect('/cm/viewclasslist/')

	else:
		return HttpResponse("Access Denied")


def getstudentfromthisclass(request):
	return HttpResponse(request.POST['allocate_from'])

def allocateStudentsToClass(request):
	if request.method == "GET":
		val_dic = {}

		allocate_from = request.GET.get('allocate_from','all')
		allocate_to = request.GET.get('allocate_to','all')


		if allocate_to == "all":
			val_dic['studentClassAllocationForm'] = StudentClassAllocationForm(1)
		
		else:
			val_dic['studentClassAllocationForm'] = StudentClassAllocationForm(1)
		
		if allocate_from == "all":
			student_list = Profile.objects.filter(who_is_this = STUDENT)
			val_dic['student_list'] = student_list
			# val_dic['studentClassAllocationForm'] = studentClassAllocationForm

		else:
			class_ = SchoolClass.objects.get(pk = allocate_from)
			students_from_class = ClassStudentAllocation.objects.filter(class_of_student = class_)
			student_list = []
			for e in students_from_class:
				student_list.append(e.student)
	
			val_dic['student_list'] = student_list
			# val_dic['studentClassAllocationForm'] = studentClassAllocationForm

			




		return render(request,"cm/studentclassallocation.html",val_dic)


	elif request.method == "POST":
		select_students_list = request.POST.getlist('selected_students')
		class_number = request.POST['class_number']
		errors = False
		if (len(select_students_list) > 0) and int(class_number) >0:
			try:
				class_of_student = SchoolClass.objects.get(pk = class_number)
				for i in select_students_list:

					ClassStudentAllocation.objects.update_or_create(class_of_student = class_of_student,student = Profile.objects.get(pk = i) , roll_number = 0)


			except Exception as e:
				errors = True


		if errors:
			return HttpResponse("errors occoured")
		else:
			return HttpResponse("Succeess")

		# return HttpResponse(select_students_list)
	else:
		return HttpResponse("Access Denied")

def viewClassList(request):
	val_dic = {}
	classList = SchoolClass.objects.select_related().order_by('starting_year')
	for i in  classList:
		i.no_of_students_in_class = ClassStudentAllocation.objects.filter(class_of_student =  i).count()


	val_dic['classList'] = classList
	return render(request,"cm/viewclasslist.html",val_dic)

def classdetails(request,pk):
	val_dic = {}
	classdetail = SchoolClass.objects.get(pk = pk)
	students_of_class = ClassStudentAllocation.objects.filter(class_of_student = classdetail)
	val_dic['classdetail'] = classdetail
	val_dic['students_of_class'] = students_of_class
	val_dic['class_strength'] = len(students_of_class)

	return render(request,"cm/classdetails.html",val_dic)


def studentprofile(request,pk):
	val_dic = {}

	student = Profile.objects.get(pk = pk)
	if student.who_is_this != STUDENT:
		return HttpResponse("Access Denied")
	val_dic['student'] = student
	return render(request,"cm/studentprofile.html",val_dic)

def getClassNameAndIDForPopulatingOptions(request):
	if request.method == "POST":
		return HttpResponse("POS")
	else:
		return HttpResponse("GET")




