from django.conf.urls import url,include
from django.contrib import admin


from .views import *

urlpatterns = [

    url(r'^newclass/$',addnewclass, name = "addnewclass"),
    url(r'^allocatestudentstoclass/$',allocateStudentsToClass, name = "allocateStudentsToClass"),
    url(r'^getstudentfromthisclass/$',getstudentfromthisclass, name = "getstudentfromthisclass"),
    url(r'^viewclasslist/$',viewClassList, name = "viewClassList"),

    url(r'^classdetails/$',classdetails, name = "classdetails"),
    
    url(r'^classdetails/(?P<pk>[0-9]+)/$',classdetails),
    url(r'^studentprofile/(?P<pk>[0-9]+)/$',studentprofile, name = "studentprofile"),




    # url(r'^newteacher/$',addteacheruser, name = "addteacheruser"),
    # url(r'^editstudentprofile/(?P<pk>[0-9]+)/$',editstudentprofile),
    # url(r'^editstudentprofile/$',editstudentprofile ,name = "editstudentprofile"),
    # url(r'^viewallstudents/$',viewallstudents,name = "viewallstudents"),
    # url(r'^viewallteachers/$',viewallteachers, name = "viewallteachers"),


    # url(r'^$',homepage),

]
