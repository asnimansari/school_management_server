from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from usermanagement.models import Profile


# class_name_choice = [('LKG','LKG'),('UKG','UKG')]#,'UKG','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII')
class_name_choice = [('LKG','LKG'),('UKG','UKG'),('I','I'),('II','II'),('III','III'),('IV','IV'),('V','V'),('VI','VI'),('VII','VII'),('VIII','VIII'),('IX','IX'),('X','X'),('XI','XI'),('XII','XII')]#,'I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII')

class SchoolClass(models.Model):
	starting_year = models.IntegerField()
	ending_year = models.IntegerField()
	class_name = models.CharField(max_length=5, choices=class_name_choice)
	division_name = models.CharField(max_length=1)
	class_teacher = models.ForeignKey(Profile,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	has_roll_list_genrated = models.BooleanField(default = False)

	def __str__(self):
		return str(self.starting_year) + " - " + str(self.ending_year) + " " + str(self.class_name) + " " + self.division_name

class ClassStudentAllocation(models.Model):
	class_of_student = models.ForeignKey(SchoolClass,on_delete=models.CASCADE)
	student = models.ForeignKey(Profile,on_delete=models.CASCADE)
	roll_number = models.IntegerField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
