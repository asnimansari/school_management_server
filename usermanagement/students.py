from django.shortcuts import render

from django.http import HttpResponse,HttpResponseRedirect
import datetime
from django.contrib.auth.models import User

from .forms import ProfileForm,UserForm
from .models import Profile
from django.template import RequestContext

from general.constants import *

from django import forms

from django.contrib import messages

def doesUserNameExists(username):

    try:
        User.objects.get(username = username)
        return True

    except:
        print(username + " Already Exists")
        return False


def addStudent(request):
    if request.method == "POST":

        # raise forms.ValidationError("This  is  Inavqs")

        # messages.error(request,"USER NOT CREATED")
        context_dic = {}
        user_form =  UserForm(request.POST)
        profile_form = ProfileForm(request.POST)


        if user_form.is_valid() and  profile_form.is_valid():
            user =  user_form.save(commit = False)
            if (doesUserNameExists(user.username.strip())):
                # messages.error(request,"INVALID  USER FORM")
                context_dic = {}
                context_dic['user_form'] = UserForm(request.POST)
                context_dic['profile_form'] = ProfileForm(request.POST)
                print("HERE")                
                return render(request,"um/addstudent.html",context_dic)
                

            else:
                user_form =  user_form.save(commit = False)
                user_form.set_password(user_form.username)

                user_form.save()

                profile = profile_form.save(commit = False)
                profile.user = user_form
                profile.who_is_this = STUDENT
                profile.save()
                return  HttpResponseRedirect('/um/success/')
        else:
            print(request.POST)
            user =  request.POST['username']

            
            if (doesUserNameExists(user)):
                messages.error(request,"INVALID  USER FORM")
                context_dic = {}
                context_dic['user_form'] = UserForm(request.POST)
                context_dic['profile_form'] = ProfileForm(request.POST)              
                return render(request,"um/addstudent.html",context_dic)
            
            
            return render(request,"um/addstudent.html",context_dic)

    else:
        context_dic = {}
        context_dic['user_form'] = UserForm()
        context_dic['profile_form'] = ProfileForm()
        # return HttpResponse("GET")
        return render(request,"um/addstudent.html",context_dic)






def  viewallstudents(request):
    context_dic  = {}
    context_dic['students'] = Profile.objects.filter(who_is_this = STUDENT)
    return render(request,"um/viewallstudents.html",context_dic)