from django.shortcuts import render

from django.http import HttpResponse
import datetime
from django.contrib.auth.models import User

from .forms import ProfileForm,UserForm
from .models import Profile

from django.template import RequestContext

USER_CREATED = 'USER_CREATED'
USER_ALREADY_EXISTS = 'USER_ALREADY_EXISTS'
USER_FOUND = 'USER_FOUND'
USER_NOT_FOUND = 'USER_NOT_FOUND'
def addteacheruser(request):
	if request.method == "POST":
		userForm = UserForm(request.POST)
		user_creation_status = USER_ALREADY_EXISTS
		if userForm.is_valid():
			user = userForm.save(commit = False)
			user.set_password(user.username)
			user.save()
			profile = Profile()
			profile.user = user
			profile.who_is_this = "T"
			profile.save()

			return HttpResponse("CREATED")
		else:
			return HttpResponse("USER ALREADY EXISTS")

	else:
		now = datetime.datetime.now()
		html = "<html><body>It is now %s.</body></html>" % now

		profileform = ProfileForm()
		userForm = UserForm()
		return render(request,"newteacher.html",{
			'profileform':userForm
			})

def updateteacherprofile(request,pk):
	if request.method == 'GET':
		user_exists = USER_FOUND
		user = None
		try:
			user = User.objects.get(pk = pk)
		except Exception as e:
			user_exists = USER_NOT_FOUND

		if ((user_exists == USER_FOUND) and not(user.is_staff) and not(user.is_superuser)):
			context_dic = {}
			context_dic['username'] = user.username
			context_dic['first_name'] = user.first_name
			context_dic['last_name'] = user.last_name
			try:
				userProfile = ProfileForm(initial = Profile.objects.get(user = user).getIntialValuesForForm())
			except:
				userProfile = ProfileForm()

			context_dic['userProfile']= userProfile
			return render(request,"newteacherprofile.html",context_dic)
		else:#user_exists == USER_NOT_FOUND:
			return HttpResponse("User Not Foun")

	if request.method == 'POST':
		user_exists = USER_FOUND
		user = None
		try:
			user = User.objects.get(pk = pk)
		except Exception as e:
			user_exists = USER_NOT_FOUND
		if user_exists == USER_NOT_FOUND:
			return HttpResponse("User Not Found")
		# profileForm = ProfileForm(request.POST, instance = user)
		profileForm = ProfileForm(request.POST,instance = Profile.objects.get(user = user))

		if profileForm.is_valid():
			profile = profileForm.save(commit = False)
			profile.user = User.objects.get(pk = pk)
			profile.save()
			return HttpResponse(profileForm)




def editstudentprofile(request,pk):
	if request.method == 'GET':
		user_exists = USER_FOUND
		user = None
		try:
			user = User.objects.get(pk = pk)
		except Exception as e:
			user_exists = USER_NOT_FOUND

		if ((user_exists == USER_FOUND) and not(user.is_staff) and not(user.is_superuser)):
			context_dic = {}
			context_dic['username'] = user.username
			context_dic['first_name'] = user.first_name
			context_dic['last_name'] = user.last_name
			try:
				userProfile = ProfileForm(initial = Profile.objects.get(user = user).getIntialValuesForForm())
			except:
				userProfile = ProfileForm()

			context_dic['userProfile']= userProfile
			return render(request,"newstudentprofile.html",context_dic)
		else:#user_exists == USER_NOT_FOUND:
			return HttpResponse("User Not Foun")

	if request.method == 'POST':
		user_exists = USER_FOUND
		user = None
		try:
			user = User.objects.get(pk = pk)
		except Exception as e:
			user_exists = USER_NOT_FOUND
		if user_exists == USER_NOT_FOUND:
			return HttpResponse("User Not Found")
		profileForm = ProfileForm(request.POST,instance = Profile.objects.get(user = user))
		if profileForm.is_valid():
			profile = profileForm.save()
			print(str(profile))

			existing_profile = Profile.objects.get(user = user)
			existing_profile.address_1 = profile.address_1
			existing_profile.address_2 = profile.address_2
			existing_profile.user_id = user.id
			existing_profile.save()
			print(existing_profile)

			return HttpResponse(profileForm)
def homepage(request):
	return render(request,"um/home.html")

def dashboard(request):
	return render(request,"um/home.html")
