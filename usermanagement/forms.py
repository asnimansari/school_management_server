from .models import Profile
from django.forms import *

from django.contrib.auth.models import User

class ProfileForm(ModelForm):
	mobile_number2 = CharField(required = False)
	address_2 = CharField(required = False)
	dob = DateField(widget = widgets.SelectDateWidget(years=[y for y in range(2000,2050)]))

	
	
	class Meta:
		model = Profile
		fields = (
	'mobile_number1',
	'mobile_number2',
	'address_1',
	'address_2',
	'locality',
	'pincode',
	'dob'

			)
class UserForm(ModelForm):
	class Meta:
		model = User
		fields = (
			'username',
			'first_name',
			'last_name',
			)
