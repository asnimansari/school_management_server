from django.db import models

from django.contrib.auth.models import User

class Profile(models.Model):
	user = models.OneToOneField(User,on_delete = models.CASCADE,related_name = 'user')
	mobile_number1 = models.CharField(max_length = 20,default = "")
	mobile_number2 = models.CharField(max_length = 20,default = "")
	address_1 = models.CharField(max_length = 40,default = "")
	address_2 = models.CharField(max_length = 40,default = "")
	locality = models.CharField(max_length = 20,default = "")
	pincode = models.CharField(max_length = 7,default = "")
	who_is_this = models.CharField(max_length = 20,default = "S")
	dob = models.CharField(max_length = 40,blank = True,null = True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.user.first_name +" "+ self.user.last_name

	def getIntialValuesForForm(self):
		val_dic = {}
		val_dic['mobile_number1'] = self.mobile_number1
		val_dic['mobile_number2'] = self.mobile_number2
		val_dic['address_1'] = self.address_1
		val_dic['address_2'] = self.address_2
		val_dic['locality'] = self.locality
		val_dic['pincode'] = self.pincode

		return val_dic
