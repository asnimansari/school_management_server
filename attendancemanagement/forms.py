

from  django.db import models

from django import forms
from django.forms import ModelForm
from datetime import date
class DateInput(forms.DateInput):
    input_type = 'date'
  
class DateOfAttendance(forms.Form):
    date  = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date','min':'2018-01-01','value':date.today()}))
    
    