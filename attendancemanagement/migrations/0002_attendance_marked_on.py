# Generated by Django 2.0 on 2017-12-26 20:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendancemanagement', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='attendance',
            name='marked_on',
            field=models.DateField(blank=True, null=True),
        ),
    ]
