from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
# Create your views here.

from .forms import *

from datetime import date

from classmanagement.models import SchoolClass,ClassStudentAllocation
import collections
from .models import *
def markAttendance(request, pk):

    # print(pk)
    if request.method == "GET":
        context = {}
        try:
            class_and_students = ClassStudentAllocation.objects.all()
            context['students'] = class_and_students
            for student in  class_and_students:
                print(student.student)


        except SchoolClass.DoesNotExist:
            return HttpResponseRedirect('/cm/viewclasslist/')
        context['dateofAttendance'] = DateOfAttendance()
        return render(request,'am/markattendance.html',context=context)

    elif request.method == "POST":
        id_and_status_of_students = processQueryDict(request.POST)
        date_of_attending  =  request.POST['date']
        class_id = pk


        for key, value  in  id_and_status_of_students.items():
            print(key  + "  " +value)
            try:
                Attendance.objects.get(
                    student = Profile.objects.get(id = key),
                    date = date_of_attending,
                    # status = value,
                    # marked_by =  User.objects.get(id =1),
                    # marked_on = date.today()
                    )

            except Attendance.DoesNotExist:

                Attendance.objects.create(
                    student = Profile.objects.get(id = key),
                    date = date_of_attending,
                    status = value,
                    marked_by =  User.objects.get(id =1),
                    marked_on = date.today()
                    )

        return HttpResponse("Succeess")




# class Attendance(models.Model):
#     student = models.ForeignKey(Profile, on_delete=models.CASCADE)
#     date = models.DateField()
#     status = models.CharField(max_length=10)
#     marked_by = models.ForeignKey(User, on_delete=models.CASCADE)
#     marked_on = models.DateField(null = True,blank = True)

def view_class_list(request):  
    classes = SchoolClass.objects.all()

    context = {}
    context['classes'] = classes

    return render(request,'am/markattendance.html',context = context)


def processQueryDict(data):

    normal_dict = data.dict()
    del normal_dict['csrfmiddlewaretoken']
    del normal_dict['date']

    normal_dict = collections.OrderedDict(normal_dict)

    return normal_dict