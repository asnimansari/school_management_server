from django.conf.urls import url,include
from django.contrib import admin


from .views import *

# from .students import addStudent,viewallstudents
# from .teachers import addTeacher,viewallteachers


urlpatterns = [

    # url(r'^newstudent/$',addStudent, name = "addstudentuser"),
    url(r'^markattendance/(?P<pk>[0-9]+)/$',markAttendance),
    url(r'^viewclasslist/',view_class_list)
    # url(r'^newteacher/$',addTeacher, name = "addteacheruser"),
    # url(r'^editstudentprofile/(?P<pk>[0-9]+)/$',editstudentprofile),
    # url(r'^editstudentprofile/$',editstudentprofile ,name = "editstudentprofile"),
    # url(r'^viewallstudents/$',viewallstudents,name = "viewallstudents"),
    # url(r'^viewallteachers/$',viewallteachers, name = "viewallteachers"),



]
