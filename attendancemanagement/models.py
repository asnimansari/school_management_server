from django.db import models

# Create your models here.
from usermanagement.models import Profile
from django.contrib.auth.models import User

class Attendance(models.Model):
    student = models.ForeignKey(Profile, on_delete=models.CASCADE)
    date = models.CharField(max_length=10)
    status = models.CharField(max_length=10)
    marked_by = models.ForeignKey(User, on_delete=models.CASCADE)
    marked_on = models.DateField(null = True,blank = True)
    

