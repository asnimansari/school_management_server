"""school_management_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin


from usermanagement import views,urls
from classmanagement import urls
from attendancemanagement import urls


from general.views import homepage
urlpatterns = [

    url(r'^$',homepage),
    url(r'^admin/', admin.site.urls),
    url(r'^dashboard/$',views.dashboard),
    url(r'^um/',include('usermanagement.urls')),
    url(r'^cm/',include('classmanagement.urls')),
    url(r'^am/',include('attendancemanagement.urls')),
    

    # url(r'^createstudentprofile/(?P<pk>[0-9]+)/',views.updatestudentprofile)
]
